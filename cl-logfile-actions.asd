(defsystem :cl-logfile-actions
  :description "Run actions when logfiles match some conditions"
  :license "LGPL3"
  :author "Philipp Marek <philipp@marek.priv.at>"
  :depends-on (:iterate
                  :cl-interpol
                  :uiop
                  :osicat
                  :fiveam
                  :alexandria)
  :around-compile (lambda (thunk)
                    (cl-interpol:enable-interpol-syntax)
                    (funcall thunk))
  :serial t
  :components ((:module "src"
                :serial T
                :components
                ((:file "package")
                 (:file "read-config")
                 (:file "logfile-reader")
                 (:file "main")
                 ))))

