(in-package :cl-logfile-actions)

;; Reading a logfile has a few important "edge" cases.
;;
;; - The file might get rotated
;; - The file might become truncated
;; - Upon an error NUL bytes might come up
;; - It might not be UTF8
;;
;; - The logfile reading process should restart at the last position,
;;   but must take care about the points above!
;;
;; We need to handle all these cases.
;;
;;
;; Furthermore,
;;
;; - we shouldn't use too much CPU - so avoid polling, if possible.



(defun get-a-file-ID (path)
  "Returns the block device and inode; we want to be sure that this is the same 
  file as last time.
  Note: this might break on NFS (not a usecase now, only local files).
  Note: No idea about windows compatibility."
  (let ((stat (osicat-posix:stat path)))
    (with-slots (stat-ino) stat
      (format nil "0x~x:~d" 
              (osicat-posix:stat-dev stat)
              (osicat-posix:stat-ino stat)))))


(defun continue-logfile-from (path byte-offset)
  (iter (until *quit*)
    (sleep 0.1)))


(defun read-logfile (path)
  (let ((file-id (get-a-file-ID path)))
    ;; compare to last-known position
    (continue-logfile-from path 0)))
